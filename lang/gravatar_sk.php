<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_descr_gravatar' => 'Nastavte vzhľad gravatarov',
	'cfg_titre_gravatar' => 'Gravatary',

	// E
	'explication_image_defaut' => 'Pre tých, ktorí nemajú žiaden avatar, používať:',

	// G
	'gravatar_info' => 'Ak chcete, aby sa fotka vašej tváre zobrazila tu, najprv ju uložte na <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (bezplatné a bezbolestné).',
	'gravatar_info_forum' => 'Ak chcete, aby sa fotka vašej tváre zobrazila vo vašej správe, najprv ju uložte na <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (bezplatné a bezbolestné) a nezabudnite tu pridať svoju e-mailovú adresu.',

	// L
	'label_image_defaut' => 'Predvolený obrázok',
	'label_image_defaut_404' => 'žiaden obrázok',
	'label_image_defaut_gravatar' => 'logo Gravatar',
	'label_image_defaut_identicon' => '<i>Identikona</i> (vytvára sa cez e-mailovú adresu)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (vytvára sa cez e-mailovú adresu)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_retro' => '<i>Retro</i> (generovaný z e-mailovej adresy)',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (vytvára sa cez e-mailovú adresu)',
	'label_taille' => 'Veľkosť gravatarov',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
