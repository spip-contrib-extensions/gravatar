<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_descr_gravatar' => 'Counfiguràs la representacioun dei gravatar',
	'cfg_titre_gravatar' => 'Gravatar',

	// E
	'explication_image_defaut' => 'Per aquelu que noun an d’avatar, utilisà :',

	// G
	'gravatar_info' => 'Per afichà lou vouòstre avatar aquì, registras lou denant soubre <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (facil e gràtis).',
	'gravatar_info_forum' => 'Per afichà lou vouòstre avatar emb’au vouòstre message, registras lou denant soubre <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (facil e gràtis) e noun oublidès d’endicà la vouòstra adressa e-mail aquì.',

	// L
	'label_image_defaut' => 'Image predefinida',
	'label_image_defaut_404' => 'mìnga image',
	'label_image_defaut_gravatar' => 'lògou Gravatar',
	'label_image_defaut_identicon' => '<i>Identicon</i> (generat d’après l’adressa e-mail)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (generat d’après l’adressa e-mail)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_retro' => '<i>Retro</i> (generat d’après l’adressa e-mail)',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (generat d’après l’adressa e-mail)',
	'label_taille' => 'Talha dei gravatar',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
