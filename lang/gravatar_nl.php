<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_descr_gravatar' => 'Configureer de weergave van gravatars',
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'Wanneer je geen gebruikersafbeelding hebt, gebruik je:',

	// G
	'gravatar_info' => 'Om hier je gebruikersafbeelding te tonen moet je je eerst registreren op <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratis en pijnloos).',
	'gravatar_info_forum' => 'Om je gebruikersafbeelding bij je bericht te tonen moet je je eerst registreren op<a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratuit et indolore). Vergeet niet om hier je e-mailadres te vermelden.',

	// L
	'label_image_defaut' => 'Standaardafbeelding',
	'label_image_defaut_404' => 'geen afbeelding',
	'label_image_defaut_gravatar' => 'Gravatar logo',
	'label_image_defaut_identicon' => '<i>Identicon</i> (gegenereerd uit het e-mailadres)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (gegenereerd uit het e-mailadres)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_retro' => '<i>Retro</i> (gemaakt n.a.v. emailadres)',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (gegenereerd uit het e-mailadres)',
	'label_taille' => 'Afmeting van de gebruikersafbeelding',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
