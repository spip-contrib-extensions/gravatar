<?php

/**
 *
 * Gravatar : Globally Recognized AVATAR
 *
 * @package     plugins
 * @subpackage  gravatar
 *
 * @author      Fil, Cedric, Thomas Beaumanoir
 * @license     GNU/GPL
 *
 * @version     $Id$
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// taille max des gravatars à récupérer sur le site
if (!defined('_TAILLE_MAX_GRAVATAR')) {
	define('_TAILLE_MAX_GRAVATAR', 80);
}

// le host vers gravatar
if (!defined('_GRAVATAR_HOST')) {
	define('_GRAVATAR_HOST', 'http://www.gravatar.com');
}

// les caches
if (!defined('_GRAVATAR_CACHE_DELAY_REFRESH')) {
	define('_GRAVATAR_CACHE_DELAY_REFRESH', 3600 * 24); // 24h pour checker un existant
}
if (!defined('_GRAVATAR_CACHE_DELAY_CHECK_NEW')) {
	define('_GRAVATAR_CACHE_DELAY_CHECK_NEW', 3600 * 8); // 8h pour re-checker un user sans gravatar
}
if (!defined('_GRAVATAR_CACHE_DELAY_LOCK')) {
	define('_GRAVATAR_CACHE_DELAY_LOCK', 3600 * 23); // 24h si gravatar nous a locke
}

/**
 * notre fonction de recherche de logo
 *
 * @deprecated obsolete, on la garde pour ne pas planter les squelettes non recalcules
 * @param  string $email  Le mail qui sert a recuperer l'image sur gravatar.com
 * @return array          Le logo de l'utilisateur
 */
function calcule_logo_ou_gravatar($email) {
	$a = func_get_args();
	$email = array_shift($a);

	// la fonction normale
	$c = call_user_func_array('calcule_logo', $a);

	// si elle repond pas, on va chercher le gravatar
	if (!$c[0]) {
		$c[0] = gravatar($email);
	}

	return $c;
}

/**
 * Produire une image x2 du gravatar dans la mesure du possible
 * @param $img
 * @param $size
 * @param $alt
 * @param $class
 * @return string
 */
function gravatar_balise_imgx2($img, $size, $alt = '', $class = '') {
	include_spip('inc/filtres');
	$taille = taille_image($img);
	list($hauteur,$largeur) = $taille;
	if (!$hauteur || !$largeur) {
		return '';
	}

	$sizex2 = $size * 2;
	if ($largeur > $sizex2 || $hauteur > $sizex2) {
		$img = filtrer('image_passe_partout', $img, $sizex2);
		$img = filtrer('image_recadre', $img, '1:1', '-', 'focus');
		$img = filtrer('image_graver', $img);
		$taille = taille_image($img);
		list($hauteur,$largeur) = $taille;
	}
	if (strpos($img, '<img') !== false) {
		$img = extraire_attribut($img, 'src');
	}
	if ($hauteur > $largeur) {
		$largeur_aff = round($size / $hauteur * $largeur);
		$hauteur_aff = $size;
	} else {
		$hauteur_aff = round($size / $largeur * $hauteur);
		$largeur_aff = $size;
	}

	// envoyer un width='..' et height='...' faux trompe les filtres images, on triche avec un style inline sur le width
	// ne doit pas perturber le responsive qui stylera avec un max-width, mais peut etre ennuyant parfois...
	// TODO : tester la version de SPIP pour se débarasser de ce fix quand c'est possible
	return '<img src="' . attribut_html($img) . "\" width=\"$largeur\" height=\"$hauteur\""
	  . ($largeur_aff < $largeur ? " style=\"width:{$largeur_aff}px;height:auto;\"" : '')
	  . ' alt="' . attribut_html($alt) . '"'
	  . ($class ? ' class="' . attribut_html($class) . '"' : '')
	  . ' />';
}

/**
 * Construit la balise HTML <img> affichant le gravatar
 * @deprecated
 * @param  string $img    Chemin de l'image
 * @param  string $alt    Texte alternatif
 * @param  string $class  Classe facultativ
 * @return string         Le code HTML
 */
function gravatar_balise_img($img, $alt = '', $class = '') {
	$taille = taille_image($img);
	list($hauteur,$largeur) = $taille;
	if (!$hauteur || !$largeur) {
		return '';
	}
	return
	"<img src='$img' width='$largeur' height='$hauteur'"
	  . " alt='" . attribut_html($alt) . "'"
	  . ($class ? " class='" . attribut_html($class) . "'" : '')
	  . ' />';
}


/**
 * pour 2.1 on se contente de produire une balise IMG
 *
 * @param  string $email        le mail qui sert a recuperer l'image sur gravatar.com
 * @param  string $logo_auteur  Le logo de l'auteur s'il existe
 * @param  bool $force          forcer le refresh du gravatar
 * @return string               La balise IMG
 */
function gravatar_img($email, $logo_auteur = '', $force = false) {
	include_spip('inc/config');
	$config = (function_exists('lire_config') ? lire_config('gravatar') : unserialize($GLOBALS['meta']['gravatar']));
	$config = (is_array($config) ? $config : []);
	$default = '404'; // par defaut rien si ni logo ni gravatar (consigne a passer a gravatar)
	$image_default = ''; // image
	$size = _TAILLE_MAX_GRAVATAR;
	if (!empty($config['taille'])) {
		$size = $config['taille'];
	}

	if (
		$config
		&& strlen($image_default = $config['image_defaut'])
		&& strpos($image_default, '.') === false
	) {
		$default = $image_default; // c'est une consigne pour l'api gravatar
		$image_default = (_SPIP_VERSION_ID < 40000 ? 'images/gravatar.png' : 'images/gravatar.svg');
		$image_default = ($default == '404') ? '' : $image_default; // si pas d'email, fournir quand meme une image
	}

	// retrouver l'image du mieux qu'on peut :
	// logo_auteur si il existe
	// ou gravatar si on a un email et si on trouve le gravatar
	if (!$img = $logo_auteur) {
		if (!$g = gravatar($email, $default, $force)) { // chercher le gravatar etendu pour cet email
			$img = '';
		} else {
			$img = gravatar_balise_imgx2($g, $size, '', 'spip_logo spip_logos photo avatar');
		}
	} else {
		// quand il y a un logo auteur on le garde intact, sans le resizer
		// mais changer la class du logo auteur
		$img = inserer_attribut($img, 'class', 'spip_logo spip_logos photo avatar');
	}

	// si pas de config, retourner ce qu'on a
	if (!$config) {
		return $img;
	}

	// ensuite le mettre en forme si les options ont ete activees
	if (
		!$img
		&& $image_default
		&& ($img = find_in_path($image_default))
	) {
		$img = gravatar_balise_imgx2($img, $size, '', 'spip_logo spip_logos photo avatar');
	}

	if (!$img) {
		return '';
	}

	return $img;
}

/**
 * Supprime un éventuel index.php
 *
 * @note
 *    Cette fonction créait un index.php pour lister les gravatars
 *    Il faut passer par ?page=debug/gravatar (identifié)
 *
 * @deprecated voir ?page=debug/gravatar
 * @staticvar boolean $done  True si la verif a deja ete faite
 * @param     string  $tmp   Le repertoire dans lequel on posera le gravatar
 * @return    void
 */
function gravatar_verifier_index($tmp) {
	static $done = false;
	if ($done) {
		return;
	}
	$done = true;
	if (file_exists($tmp . 'index.php')) {
		supprimer_fichier($tmp . 'index.php');
	}
}

/**
 * Recupere l'image sur www.gravatar.com et la met en cache
 *
 * @staticvar int         $nb       le nombre max d'anciens
 * @staticvar int         $max      le nombre max de nouveaux
 * @param     string      $email    le mail qui va servir pour calculer le gravatar
 * @param     int|string  $default  gravatar par defaut : 404 ou identicon/monsterid/wavatar
 * @param     bool        $force    forcer le refresh synchrone
 * @return    null|string           le chemin du fichier gravatar, s'il existe
 */
function gravatar($email, $default = '404', $force = false) {
	static $nb = 5; // ne pas en charger plus de 5 anciens par tour
	static $max = 10; // et en tout etat de cause pas plus de 10 nouveaux

	// eviter une requete quand l'email est invalide
	if (
		!($email = trim(is_null($email) ? '' : $email))
		|| !strlen($email)
		|| !email_valide($email)
	) {
		return '';
	}

	$tmp = sous_repertoire(_DIR_VAR, 'cache-gravatar');
	$lock_file = $tmp . 'gravatar.lock';


	$md5_email = md5(strtolower($email));
	// privacy : http://archive.hack.lu/2013/dbongard_hacklu_2013.pdf
	// eviter de rendre les emails retrouvables par simple reverse sur le md5 de gravatar
	if (!isset($GLOBALS['meta']['gravatar_salt'])) {
		include_spip('inc/acces');
		$salt = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['SERVER_SIGNATURE'] . creer_uniqid();
		ecrire_meta('gravatar_salt', hash('sha256', $salt), 'non');
	}
	$gravatar_id = sha1(strtolower($email) . $GLOBALS['meta']['gravatar_salt']);

	$gravatar_default = '';
	if (in_array($default, ['404','mm','identicon','monsterid','wavatar','retro'])) {
		$gravatar_default = $default;
		$default = '';
	}
	elseif (strpos($default, '.') !== false && file_exists($default)) {
		$gravatar_default = '404';
	}
	else {
		$default = '';
	}

	$gravatar_id .= ($gravatar_default == '404' ? '' : "-$gravatar_default");
	$gravatar_cache = $tmp . $gravatar_id . '.jpg';
	$gravatar_vide = $tmp . $gravatar_id . '.vide';

	$gravatar = '';
	// On verifie si le gravatar existe en controlant la taille du fichier
	if (@filesize($gravatar_cache)) {
		if (@getimagesize($gravatar_cache)) {
			$gravatar = $gravatar_cache;
		}
		else {
			@unlink($gravatar_cache);
		}
	}
	// sinon si default est un chemin d'image, le prendre en fallback
	if (!$gravatar && $default) {
		$gravatar = $default;
	}

	// si on est locke, on utilise ce qu'on a
	if (
		file_exists($lock_file)
		&& $_SERVER['REQUEST_TIME'] - filemtime($lock_file) < _GRAVATAR_CACHE_DELAY_LOCK
	) {
		return $gravatar;
	}

	// si on a un cache valide, on l'utilise
	if ($gravatar == $gravatar_cache && !$force) {
		$duree = $_SERVER['REQUEST_TIME'] - filemtime($gravatar_cache);
		if ($duree < _GRAVATAR_CACHE_DELAY_REFRESH || $nb-- <= 0) {
			return $gravatar;
		}
		spip_log("Actualiser gravatar existant $email anciennete $duree s (cache maxi " . _GRAVATAR_CACHE_DELAY_REFRESH . 's)', 'gravatar');
		@touch($gravatar_cache); // un touch pour eviter une autre mise a jour concurrente
	}
	// si c'est un email sans gravatar connu (deja verifie), on ne reverifie pas que passe un delai suffisant
	else {
		// si un fichier vides.txt existe encore, le transformer en touch unitaires
		lire_fichier($tmp . 'vides.txt', $vides);
		if ($vides && $vides = @unserialize($vides)) {
			foreach ($vides as $id => $t) {
				@touch($tmp . $id . '.vide', $t);
			}
			@unlink($tmp . 'vides.txt');
		}

		if (file_exists($gravatar_vide)) {
			$duree_vide = $_SERVER['REQUEST_TIME'] - filemtime($gravatar_vide);
			if ($duree_vide < _GRAVATAR_CACHE_DELAY_CHECK_NEW || $nb-- <= 0) {
				return $gravatar;
			}
			// un actualise un gravatar vide que si c'est celui du visiteur identifie
			if (
				$force
				|| (isset($GLOBALS['visiteur_session']['email']) && $GLOBALS['visiteur_session']['email'] === $email)
				|| (isset($GLOBALS['visiteur_session']['session_email']) && $GLOBALS['visiteur_session']['session_email'] === $email)
			) {
				spip_log("Actualiser gravatar vide $email $duree_vide s (cache maxi " . _GRAVATAR_CACHE_DELAY_CHECK_NEW . 's)', 'gravatar');
				@touch($gravatar_vide); // un touch pour eviter une autre mise a jour concurrente
			}
			else {
				return $gravatar;
			}
		}
		else {
			spip_log("Recherche nouveau gravatar $email", 'gravatar');
		}
	}

	// pas trop de requetes sur un seul tour
	if ($max-- <= 0) {
		return $gravatar;
	}

	include_spip('inc/distant');
	spip_timer('gravatar');
	$url_gravatar = _GRAVATAR_HOST
		. '/avatar/'
		. $md5_email
		. '.jpg'
		. ($gravatar_default ? "?d=$gravatar_default" : '')
		. '&s=' . (2 * _TAILLE_MAX_GRAVATAR);

	// recuperation OK ?
	$GLOBALS['inc_distant_allow_fopen'] = false; // pas de fallback sur fopen si on est bloque par gravatar.com
	$gravatar_bin = false;
	$res = recuperer_url($url_gravatar);
	if ($res['status'] < 300 && !empty($res['page'])) {
		$gravatar_bin = $res['page'];
	}
	unset($GLOBALS['inc_distant_allow_fopen']);
	unset($res);
	$dt = spip_timer('gravatar', true);
	if ($gravatar_bin) {
		spip_log('recuperer gravatar OK pour ' . $email, 'gravatar');
		ecrire_fichier($gravatar_cache, $gravatar_bin);
		// si c'est un png, le convertir en jpg
		$a = @getimagesize($gravatar_cache);
		if (!$a) {
			$gravatar_bin = false;
		}
		else {
			// png ?
			if ($a[2] == IMAGETYPE_PNG) {
				// pour eviter un warning sous windows si le fichier existe deja
				if (file_exists($gravatar_cache . '.png')) {
					@unlink($gravatar_cache . '.png');
				}
				rename($gravatar_cache, $gravatar_cache . '.png');
				include_spip('inc/filtres_images');
				include_spip('inc/filtres_images_mini');
				$img = imagecreatefrompng($gravatar_cache . '.png');
				_image_imagejpg($img, $gravatar_cache);
			}
			else {
				if (file_exists($gravatar_cache . '.png')) {
					@unlink($gravatar_cache . '.png');
				}
			}
			if (file_exists($gravatar_vide)) {
				@unlink($gravatar_vide);
			}

			if ($gravatar !== $gravatar_cache) {
				gravatar_verifier_index($tmp);
				$gravatar = $gravatar_cache;
			}
		}
	}
	if (!$gravatar_bin) {
		// si ca a ete trop long, ne pas ressayer (IP serveur ban par gravatar ?)
		if ($dt > 10000) {
			$nb = 0;
			@touch($lock_file);
			spip_log("gravatar.com trop long a repondre pour $email ($dt), on lock $lock_file", 'gravatar');
		}
		else {
			spip_log('gravatar vide pour ' . $email, 'gravatar');
		}
		// si on a pas eu de reponse mais qu'un cache existe le prolonger pour eviter de rechecker tout le temps
		if ($gravatar === $gravatar_cache) {
			@touch($gravatar_cache);
		}
		else {
			@touch($gravatar_vide);
		}
	}

	return $gravatar;
}
