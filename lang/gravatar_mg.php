<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_descr_gravatar' => 'Configurez le rendu des gravatars',
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'Pour ceux qui n’ont pas d’avatar, utiliser :',

	// G
	'gravatar_info' => 'Pour afficher votre trombine ici, enregistrez-la d’abord sur <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratuit et indolore).',
	'gravatar_info_forum' => 'Pour afficher votre trombine avec votre message, enregistrez-la d’abord sur <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratuit et indolore) et n’oubliez pas d’indiquer votre adresse e-mail ici.',

	// L
	'label_image_defaut' => 'Image par defaut',
	'label_image_defaut_404' => 'aucune image',
	'label_image_defaut_gravatar' => 'logo Gravatar',
	'label_image_defaut_identicon' => '<i>Identicon</i> (généré d’après l’adresse email)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (généré d’après l’adresse email)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_retro' => '<i>Retro</i> (généré d’après l’adresse email)',
	'label_image_defaut_svg' => '<tt>images/gravatar.svg</tt>',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (généré d’après l’adresse email)',
	'label_taille' => 'Taille des gravatars',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
