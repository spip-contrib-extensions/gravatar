<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gravatar?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// G
	'gravatar_description' => 'Enables to use a cache system to store the gravatars.
_ To use it in a loop in this manner : <code>#GRAVATAR{email, size, URL of the default image}</code>
_ Example : <code>#GRAVATAR{#EMAIL,80,#URL_SITE_SPIP/defaut-gravatar.gif}</code>

Also extend the #LOGO_AUTEUR tag in order to take into account the gravatar of an author if it exists, including in forums and petitions.
_ Configures a default image, and image size.', # MODIF
	'gravatar_slogan' => 'Display Gravatar for author or forum poster',
];
