<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gravatar?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// G
	'gravatar_slogan' => 'Zobrazí gravatar autora alebo prispievateľa diskusného fóra',
];
