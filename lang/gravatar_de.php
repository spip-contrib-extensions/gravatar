<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_descr_gravatar' => 'Benutzen Sie das Gravatar-Rendering',
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'Für alle, die keinen Gravatar haben:',

	// G
	'gravatar_info' => 'Um Ihren Avatar hier anzeigen zu lassen, registrieren Sie sich erst hier <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (kostenlos und einfach).',
	'gravatar_info_forum' => 'Um Ihren Avatar hier anzeigen zu lassen, registrieren Sie sich erst hier <a href="http://www.gravatar.com/" rel="external nofollow" hreflang="en" class="spip_out">gravatar.com</a> (kostenlos und einfach). Vergessen Sie nicht, hier Ihre E-Mail-Adresse einzutragen.',

	// L
	'label_image_defaut' => 'Standardbild',
	'label_image_defaut_404' => 'Kein Bild',
	'label_image_defaut_gravatar' => 'Gravatar-Logo',
	'label_image_defaut_identicon' => '<i>Identicon</i> (von der E-Mail-Adresse abgeleitet)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (von der E-Mail-Adresse abgeleitet)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_retro' => '<i>Retro</i> (von der Mailadresse abgeleitet)',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (von der E-Mail-Adresse abgeleitet)',
	'label_taille' => 'Größe des Gravatars',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
