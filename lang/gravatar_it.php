<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/gravatar?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'Per coloro che non hanno Gravatar, usa:',

	// G
	'gravatar_info' => 'Per mostrare qui il tuo avatar, registralo prima su <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratis e indolore).',
	'gravatar_info_forum' => 'Per mostrare qui il tuo avatar, registralo prima su <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (gratis e indolore). Non dimenticare di fornire il tuo indirizzo email.',

	// L
	'label_image_defaut' => 'immagine standard',
	'label_image_defaut_404' => 'Non c’� immagine',
	'label_image_defaut_gravatar' => 'Logo di Gravatar',
	'label_image_defaut_identicon' => '<i>Identicon</i> (generato dall’indirizzo email)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (generato dall’indirizzo email)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (generato dall’indirizzo email)',
	'label_taille' => 'Dimensioni dei Gravatar',

	// T
	'titre_gravatar_auteur' => 'Gravatar',
];
